package ru.zhdanovpasha.laba3

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_main.*
import android.graphics.Bitmap
import android.widget.Button
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import java.util.*


class MainActivity : AppCompatActivity() {

    val NAME_KEY: String = "name"
    private val INPUT_CORRECT_NAME: String = "Your name must be non-empty"
    private lateinit var editText: EditText
    private lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //this code to change locale to english
//        val locale = Locale("en")
//        val config = baseContext.resources.configuration
//        config.locale = locale
//        baseContext.resources.updateConfiguration(config, baseContext.resources.displayMetrics)

        setContentView(R.layout.activity_main)

        editText = edit_text
        button = btn

        if (savedInstanceState != null){
            editText.setText(savedInstanceState.getString(NAME_KEY))
        }

        button.setOnClickListener({
            val text: String = editText.text.toString()
            if (text.isEmpty()){
                Toast.makeText(this, getString(R.string.non_empty_name), LENGTH_SHORT).show()
            } else {
                val intent = Intent(this, ResultActivity::class.java)
                intent.putExtra(NAME_KEY, editText.text.toString())
                startActivity(intent)
                finish()
            }
        })
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putString(NAME_KEY, editText.text.toString())
    }

}