package ru.zhdanovpasha.laba3

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.provider.MediaStore
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_result.*
import android.R.attr.bitmap

class ResultActivity : AppCompatActivity() {

    private lateinit var imageView: ImageView
    private lateinit var textView: TextView
    private val REQUEST_IMAGE_CAPTURE = 1
    private val IMAGE_KEY: String = "image"
    private val NAME_KEY: String = "name"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        val s = intent.getStringExtra(MainActivity().NAME_KEY)
        textView = text_view
        textView.text = s
        imageView = image_view

        if (savedInstanceState != null){
            val bitmap: Bitmap = Bitmap.createBitmap(savedInstanceState.getParcelable(IMAGE_KEY))
            imageView.setImageBitmap(bitmap)
        } else {
            dispatchTakePictureIntent()
        }

        backBtn.setOnClickListener({
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        })
    }

    private fun dispatchTakePictureIntent(){
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(packageManager) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            val extras = data?.extras
            val imageBitmap = extras!!.get("data") as Bitmap
            imageView.setImageBitmap(imageBitmap)
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putString(NAME_KEY, textView.text.toString())
        if (imageView.getDrawable() == null){
            return
        }
        val bitmap = (imageView.getDrawable() as BitmapDrawable).bitmap
        outState?.putParcelable(IMAGE_KEY, bitmap)
    }

}
